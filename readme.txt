# How to ssh directly to the box
vagrant ssh-config 

# running command on line :2: will tell you how to do the following
ssh vagrant@127.0.0.1 -p2222 -i ~/code/centos/.vagrant/machines/default/virtualbox/private_key

# How to do this on debian / ubuntu.
sudo apt update && upgrade
sudo apt install python3 python3-pip ipython3
sudo apt install python3-pip
python3 --version
python -m pip install --user ansible

# Vagrant file doesn't work without vagrant plugin install vagrant-gatling-rsync
vagrant plugin install vagrant-gatling-rsync

#Getting the controller to be able to connect to the nodes was a nightmare.
#You'll need to automate this eventually
#https://devops.stackexchange.com/questions/1237/how-do-i-configure-ssh-keys-in-a-vagrant-multi-machine-setup

ansible-playbook tomcat-setup.yml
ansible tomcats -m ping

# Need to organize a bit, maybe look at what some tutorials do.
https://www.guru99.com/ansible-tutorial.html